# This code will be used to check Faster_RCNN_Results

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

# tensorflow 버전 1.4 이상
if tf.__version__ < '1.4.0':
  raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')
  
# This is needed to display the images.
# %matplotlib inline

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from utils import label_map_util
from utils import visualization_utils as vis_util


# 학습된 CNN 모델명
MODEL_NAME = 'Faster_RCNN_Results_iter=80000'

# Path to frozen detection graph. This is the actual model
# that is used for the object detection.

# graph 파일 위치
PATH_TO_CKPT ='/home/pirl/Desktop/models/research/object_detection/Faster_RCNN_Results_iter=80000/frozen_inference_graph.pb'

# labelmap 파일 위치
# korean_pear, hodu, horong....
PATH_TO_LABELS = os.path.join('/home/pirl/Desktop/models/research/object_detection/training', 'object-detection.pbtxt')

# 라벨링한 class 수
NUM_CLASSES = 7

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    try:
        serialized_graph = fid.read()
    except Exception as ex:
        print("error: ", ex)
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                               max_num_classes=NUM_CLASSES,
                                               use_display_name=True)
category_index = label_map_util.create_category_index(categories)

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

# test 할 이미지 파일의 위치
# PATH_TO_TEST_IMAGES_DIR = '/home/pirl/Desktop/models/research/object_detection/test_images'
PATH_TO_TEST_IMAGES_DIR = '/home/pirl/android_image'
TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR,
                    'image{}.jpg'.format(i)) for i in range(0,1) ]

# Size, in inches, of the output images.
IMAGE_SIZE = (20, 20)


with detection_graph.as_default():
  with tf.Session(graph=detection_graph) as sess:
    # Definite input and output Tensors for detecti4on_graph
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Each box represents a part of the image where a particular object was detected.
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    for image_path in TEST_IMAGE_PATHS:
      image = Image.open(image_path)
      # the array based representation of the image will be used later in order to
      # prepare the result image with boxes and labels on it.
#       image_np = load_image_into_numpy_arr3ay(image)
      image_np = load_image_into_numpy_array(image)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      # Actual detection.
      (boxes, scores, classes, num) = sess.run(
          [detection_boxes, detection_scores, detection_classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
      # Visualization of the results of a detection.
      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=8)
    name_copy = vis_util.get_name()

# detection한 결과의 calss 이름만 출력
    result = []
    for word in name_copy:
        spl = word.split(':')
        result.append(spl[0])
    result = list(set(result))
    for word in result:
        print(word)
