# This code will be used to check Faster_RCNN_Results
import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
# print("t1")
import tensorflow as tf
# print("t2")
import zipfile
# print("t3")
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
if tf.__version__ < '1.4.0':
  raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')
# This is needed to display the images.
# %matplotlib inline
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from utils import label_map_util
from utils import visualization_utils as vis_util
# name of customized model
################## changed
MODEL_NAME = 'Faster_RCNN_Results_iter=80000'
###### no longer necessary######
# MODEL_FILE = MODEL_NAME + '.tar.gz'
# DOWNLOAD_BASE =
# 'http://download.tensorflow.org/models/object_detection/'
###### no longer necessary######
# Path to frozen detection graph. This is the actual model
# that is used for the object detection.
################## changed
PATH_TO_CKPT ='/home/pirl/Desktop/models/research/object_detection/Faster_RCNN_Results_iter=80000/frozen_inference_graph.pb'
# actual labels that annotate on the cutomized our image
# korean_pear, hodu, horong....
################## changed
PATH_TO_LABELS = os.path.join('/home/pirl/Desktop/models/research/object_detection/training', 'object-detection.pbtxt')
NUM_CLASSES = 7
detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    try:
        # print("exe1")
        serialized_graph = fid.read()
        # print("exe2")
    except Exception as ex:
        print("error: ", ex)
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')
# print("log1")
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map,
                                               max_num_classes=NUM_CLASSES,
                                               use_display_name=True)
category_index = label_map_util.create_category_index(categories)
# print("log2")
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)
# print("log3")
# If you want to test the code with your images,
# just add path to the images to the TEST_IMAGE_PATHS.
################## changed
# PATH_TO_TEST_IMAGES_DIR = '/home/pirl/Desktop/models/research/object_detection/test_images'
PATH_TO_TEST_IMAGES_DIR = '/home/pirl/android_image'
TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR,
                    'image{}.jpg'.format(i)) for i in range(0,1) ]
# print("log4")
# Size, in inches, of the output images.
IMAGE_SIZE = (20, 20)
# print("log5")
with detection_graph.as_default():
  # print("log6")
  with tf.Session(graph=detection_graph) as sess:
    # print("log7")
    # Definite input and output Tensors for detecti4on_graph
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Each box represents a part of the image where a particular object was detected.
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    # print("log8")
    for image_path in TEST_IMAGE_PATHS:
      # print("log9",image_path)
      image = Image.open(image_path)
      # print("log10",image_path)
      # the array based representation of the image will be used later in order to
      # prepare the result image with boxes and labels on it.
#       image_np = load_image_into_numpy_arr3ay(image)
      image_np = load_image_into_numpy_array(image)
      # print("log11",image_path)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      # Actual detection.
      (boxes, scores, classes, num) = sess.run(
          [detection_boxes, detection_scores, detection_classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
      # Visualization of the results of a detection.
      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=8)
    name_copy = vis_util.get_name()
#         result = list()
    result = []
    # print("log10")
    for word in name_copy:
        spl = word.split(':')
        result.append(spl[0])
    result = list(set(result))
    for word in result:
        print(word)
